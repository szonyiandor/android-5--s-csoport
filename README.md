# Android 5-os csoport

Projekt neve: Clicker

Rövid összefoglaló:
A Clicker egy tap-game lesz. 
Lesz egy játékos karakter és ellenfelek. 
A játékos karaktere akkor támad, ha tapelünk (onTouchEvent), ettől az ellenfél 
élete csökken és ha elfogy, meghal. Az ellenfél is támad bizonyos időközönként, 
és ha a játékos élete fogy el, akkor Game Over.
Minden legyőzött ellenfél után lehetőséget kap a játékos upgradelni a 
karakterét, így egyre nagyobbat sebezhet, egyre több élete lehet.

Felhasználói funkciók:
Játék kezdése
Játék megállítása
Játék folyatása
Játék befejézse
Újrakezdés

Részletes leírás:
A játék menete úgy néz ki, hogy a képernyő bal oldalán áll a játékos karaktere,
jobb oldalon a mindenkori ellenfél, mindketten állnak egy helyben és csak
támadnak, fölöttük látható az életük, ha ez elfogy, meghalnak. A játékos minden
tapeléssel támad, sebessége a játékos idegbetegségén, erőssége az upgradeken
múlik. Az ellenfelek támadásának nagysága és sebessége, szintjüktől függ, minél
később következnek a játékban, annyival erősebbek lesznek.
Upgradelni minden ellenfél legyőzése után lesz lehetősége a játékosnak, és vagy
az életerejét tudja növelni, vagy a támadásai erősségét.

A legutóbbi verzió szerint, a játék működési szabályszerűségei:
- Tapelésre üt a játékos, 1-et sebez
- Minden legyőzött ellenfél után 0.5-el nő a játékos sebzése
- Az ellenfelek jelen állás szerint 1-et sebeznek másodpercenként 
- Az első ellenfélnek 10 élete van
- A sorban következő ellenfélnek az előzőhöz képest +20 élete van
- Ha kész a project sör + c
- Esetleg amdm értesítése egy focira
