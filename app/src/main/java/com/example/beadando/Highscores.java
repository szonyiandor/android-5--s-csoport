package com.example.beadando;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class Highscores extends AppCompatActivity {

    private Button menü;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);

        menü = (Button) findViewById(R.id.vissza2);
        menü.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                openActivityMain();
            }
        });

        // ide jön a sok kód.

    }

    public void openActivityMain() {
        Intent intent = new Intent(this, ActivityMain.class);
        startActivity(intent);
    }
}
