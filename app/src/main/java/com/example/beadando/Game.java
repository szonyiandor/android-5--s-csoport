package com.example.beadando;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class Game extends AppCompatActivity {

    private Button menü;
    private ImageButton imageButton;
    private ImageView imageView1;
    private ImageView imageView3;
    private ImageView imageView4;
    private TextView EleteroEllenseg;
    private TextView Szint;


    double sebzes;
    int szintSebzes;
    int ellensegSzintje;
    int sajatElet = 100;
    double ellensegElet = 10;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        menü = (Button) findViewById(R.id.vissza);
        menü.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                openActivityMain();
            }
        });

        EleteroEllenseg = (TextView) findViewById(R.id.eleteroEllenseg);

        imageView1 = (ImageView) findViewById(R.id.masodik);
        imageView3 = (ImageView) findViewById(R.id.harmadik);
        imageView4 = (ImageView) findViewById(R.id.negyedik);
        imageButton = (ImageButton) findViewById(R.id.elso);
        Szint = (TextView) findViewById(R.id.szint);

        imageButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    imageButton.setVisibility(View.INVISIBLE);
                    imageView1.setVisibility(View.VISIBLE);

                    ellensegElet -= sebzes + 1;

                    if (ellensegElet < 1){
                        EleteroEllenseg.setText("Level UP");
                        ellensegSzintje ++;
                        sebzes += 0.5;
                        szintSebzes += 20;
                        ellensegElet = 10 + szintSebzes;

                    }
                    else{
                        EleteroEllenseg.setText("Ellenség élete: " + ellensegElet);
                    }
                    Szint.setText("Szint: " + ellensegSzintje);
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    imageButton.setVisibility(View.VISIBLE);
                    imageView1.setVisibility(View.INVISIBLE);
                    return true;
                }
                return false;
            }
        });


        final TextView EleteroSajat;
        EleteroSajat = (TextView) findViewById(R.id.eleteroSajat);

        Thread t = new Thread(){
            boolean animacio = true;
            @Override
            public void run(){
                while(!isInterrupted()){
                    try {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                sajatElet -=1;
                                if (sajatElet < 1){
                                    EleteroSajat.setText("GAME OVER");

                                }
                                else{
                                    EleteroSajat.setText("Saját élet: " + sajatElet);
                                }

                                if (animacio == true){
                                    imageView3.setVisibility(View.INVISIBLE);
                                    imageView4.setVisibility(View.VISIBLE);
                                    animacio = false;
                                }
                                else{
                                    imageView3.setVisibility(View.VISIBLE);
                                    imageView4.setVisibility(View.INVISIBLE);
                                    animacio = true;
                                }
                            }
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        t.start();
    }

    public void openActivityMain() {
        Intent intent = new Intent(this, ActivityMain.class);
        startActivity(intent);
    }

}